package test.myandroidapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnSave;
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoSomthings();
            }
        });

    }

    private void DoSomthings() {
        EditText txtCardNo;
        txtCardNo = findViewById(R.id.txtCardNo);

        EditText txtFirstName;
        txtFirstName = findViewById(R.id.txtFirstName);

        EditText txtLastName;
        txtLastName = findViewById(R.id.txtLastName);

        Toast.makeText(this, txtCardNo.getText() + " " + txtFirstName.getText() + " " + txtLastName.getText(), Toast.LENGTH_LONG).show();
    }
}
